#include <Servo.h> 
Servo verticalServo;
Servo upperLatchServo;
Servo lowerLatchServo;
Servo rampServo;
Servo leftMotor;
Servo rightMotor;

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);
  upperLatchServo.attach(2);
  lowerLatchServo.attach(3);
  verticalServo.attach(4);
  leftMotor.attach(9);
  rightMotor.attach(10);
  rampServo.attach(5);
  leftMotor.writeMicroseconds(1000);
  rightMotor.writeMicroseconds(1000);
  delay(5000);
}

void verticalMovementServo(int Speed, int Time) {
  verticalServo.writeMicroseconds(Speed);
  delay(Time);
}
void latchServoSystem() {
  upperLatchServo.write(50);
  delay(1000);
  upperLatchServo.write(90);
  delay(1000);
  lowerLatchServo.write(40);
  delay(1000);
  lowerLatchServo.write(90);
  delay(1000);
}
void rampMovement(int Degrees) {
  rampServo.write(Degrees);
}
void leftMotorRun(int Speed) {
  leftMotor.writeMicroseconds(Speed);
}
void rightMotorRun(int Speed) {
  rightMotor.writeMicroseconds(Speed);
  }

 void stopEverything() {
  leftMotor.writeMicroseconds(1000);
  rightMotor.writeMicroseconds(1000);
  upperLatchServo.write(90);
  lowerLatchServo.write(90);
  }
 
void sample1() {
  verticalMovementServo(1520,200);
  delay(100);
  leftMotorRun(1100);
  rightMotorRun(1100);
  delay(50);
  latchServoSystem();
}
void sample2() {
  verticalMovementServo(1480,200);
  delay(100);
  leftMotorRun(1200);
  rightMotorRun(1200);
  delay(50);
  latchServoSystem();
}
String o;
int rampAngle = 90;
void loop() {
 if(Serial.available()) {
  o = Serial.readStringUntil('\n');
  Serial.println(o);
    if (o == "left") {
    verticalMovementServo(1550,100);
    delay(200);
    verticalServo.writeMicroseconds(1500);
  }
  else if (o == "right") {
    verticalMovementServo(1450,100);
    delay(200);
    verticalServo.writeMicroseconds(1500);
    }
    else if (o == "stop") {
      stopEverything();
      o = "abc";
      }
    else if (o == "up") {
      rampAngle+=5;
      rampMovement(rampAngle);
    }
    else if (o == "down") {
      rampAngle-=5;
      rampMovement(rampAngle);
      }
    
  }
   if (o == "start") {
      leftMotorRun(1080);
      rightMotorRun(1060);
      latchServoSystem();
      }
}  

  
  
  
