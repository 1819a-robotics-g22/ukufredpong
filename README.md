# Ping-Pong Thrower

# Members:
 Uku Soome |
 Fred Peter Boldin |
 Kaspar Rohtmaa |
 Renet Eres |

# Overview
Main idea

“Ping-Pong Thrower” project is exactly what the names states, it is a robot that launches a ping pong ball in different directions, speeds and angles. 
The main premise is that a person feeds a ping pong ball into the feeding tube, then the ball falls onto a latch. Next to the latch there is a sensor that detects if a ball is there or not, if it does detect a ball then the system is ready for launch. Then using a mobile application we call out the functions that we have programmed that release the latch and give the two motors the necessary spinning speed to launch the ball when it falls down the feeding tube onto the “launching area”.

How we envision the construction of the robot

The whole construction sits on top of a plexiglass platform, on that platform there is a servo that is connected to another plexiglass platform that turns left and right using the servo.
On top of that there are two plates of plexiglass and between them there is also a servo that changes the tilted angle of the system. The actual launching system is on the top plate.

On the top plate there are:

A ramp that is controlled that is controlled with a servo.
Feeding tube
Two motors that control the spinning speed of the rotors
Two spinning rotors with rubber seals around them that are attached to motors
Ras-Pi (might be on the middle plate instead of the top plate)
Breadboard (might be on the middle plate instead of the top plate)
Wiring (might be on the middle plate instead of the top plate)
Latch (inside the tube)
Sensor
Two servos that create a “chamber” that act as a stopper for the balls that are loaded into the feeding tube so that the balls fall down the tube one by one


On the bottom plate there is:

A Servo that has a plexiglass platform attached on top of it for turning the system left and right

# Schedule A (Fred & Uku)
Week 1: Robot parts design

Week 2: Assembly and if assembling is completed, programming of making the base of the robot move

Week 3: Programming the base and programming of throwing the ball

Week 4: Even more programming of the throwing, working on the poster

Week 5: Final adjustments
# Schedule B (Renet & Kaspar)
Week 1: Robot parts design 

Week 2: Cutting out and assembly

Week 3: Programming of throwing the ball

Week 4: Working on the poster

Week 5: Final adjustments
# Component list
1. Plexiglas or something similar (~0.5m^2) Price: ~9€ (goo.gl/dCcCN3)
2. Servo motor x4 - from school or when bought Price: 32€ (goo.gl/t5VtrV)
3. Rubber O seals Price: ~1.3€ (goo.gl/Rex9LW)
4. Thrower motor - from school or when bought Price: 30€ (goo.gl/DQ9BYy)
5. Raspberry Pi - from school
6. Screws - from school or when bought Price: ~3€ (goo.gl/NvcqmQ)
7. Pipe - Price: ~1€ (goo.gl/YAYpYY
8. electronic speed controller x2

Planned expenses: ~16€ | if we have to buy everything ourselves: ~83€

# Challenges
1. Getting the rubber bands to stay on the motors, not to fly off and not to slip down so it jams the motor.
2. Reliability of the all the hardware working together.
3. Programming it so it's usable by a regular person who doesn't know how to program.
# Solutions
1. Glue the rubber bands onto the motors.
2. Put in more time towards the project and test it more.
3. Use an andriod app that connects to a raspi to control the robot.