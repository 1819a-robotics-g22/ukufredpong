import tkinter as tk
import serial

LARGE_FONT = ("Verdana", 16)
try:
    ser = serial.Serial('/dev/ttyUSB0', 9600)
    print("Serial is made0")
except:
    print("Serial is not found")
try:
    ser = serial.Serial('/dev/ttyUSB1', 9600)
    print("Serial is made1")
except:
    print("Serial is not found")
try:
    ser = serial.Serial('/dev/ttyUSB2', 9600)
    print("Serial is made2")
except:
    print("Serial is not found")

class SeaofBTCapp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        frame = StartPage(container, self)

        self.frames[StartPage] = frame

        frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


def qf(quickPrint):
    ser.write(quickPrint+"\n")


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="XXXKillerBOTXXX", font=LARGE_FONT)
        label.pack(pady=30, padx=30)

        button = tk.Button(self, text="Vasakule",
                           command=lambda: qf("left"))
        button1 = tk.Button(self, text="Paremale",
                           command=lambda: qf("right"))
	button2 = tk.Button(self, text="Up",
                           command=lambda: qf("up"))
	button3 = tk.Button(self, text="Down",
                           command=lambda: qf("down"))
	button4 = tk.Button(self, text="Start",
                           command=lambda: qf("start"))
	button5 = tk.Button(self, text="Stop",
                           command=lambda: qf("stop"))

	button.pack()
    button1.pack()
	button2.pack()
	button3.pack()
	button4.pack()
	button5.pack()
app = SeaofBTCapp()
app.mainloop()
